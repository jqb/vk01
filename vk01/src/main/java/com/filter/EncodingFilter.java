package com.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingFilter implements Filter {
	private FilterConfig config;
	private String encodingType = "utf-8";

	public EncodingFilter() {
	}

	public void destroy() {
		setConfig(null);
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(encodingType);
		chain.doFilter(request, response);
	}

	public void init(FilterConfig config) throws ServletException {
		this.setConfig(config);
		String s = config.getInitParameter("encoding");
		if (s != null) {
			encodingType = s;
		}
	}

	public void setConfig(FilterConfig config) {
		this.config = config;
	}

	public FilterConfig getConfig() {
		return config;
	}
}
