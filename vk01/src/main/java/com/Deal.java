package com;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.Clothesbean;
import com.beans.Msgbean;
import com.beans.Userbean;
import com.util.DB;

public class Deal extends HttpServlet {

	private static final long serialVersionUID = -1020559801160217796L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("pwd");
		String checkcode = request.getParameter("checkCode");
		String email = request.getParameter("email");
		String deal = request.getParameter("deal");
		String info = new String();
		Userbean user = new Userbean();
		DB db = new DB();
		// System.out.println(deal);
		if (deal.equals("login")) {
			user = db.getUser(username);
			String uname = user.getUsername();
			String upwd = user.getPassword();
			if (uname == null)
				uname = "";
			if (upwd == null)
				upwd = "";
			// System.out.println(uname+","+upwd);
			// // System.out.println(username+","+password);
			// // System.out.println(user.getUsername()+","+user.getPassword());
			HttpSession session = request.getSession();
			String srand = session.getAttribute("rcheckCode").toString();
			if (!uname.equals(username)) {
				info = "用户不存在！";
				request.getRequestDispatcher(
						"fail.jsp?info=" + info + "&deal=" + deal + "")
						.forward(request, response);
			} else if (!checkcode.equals(srand)) {
				info = "验证码错误！";
				request.getRequestDispatcher(
						"fail.jsp?info=" + info + "&deal=" + deal + "")
						.forward(request, response);
			} else if (!upwd.equals(password)) {
				info = "密码错误！";
				request.getRequestDispatcher(
						"fail.jsp?info=" + info + "&deal=" + deal + "")
						.forward(request, response);
			} else {
				info = "登陆成功！";
				// session.setAttribute("user", user);
				session.setAttribute("user", user);
				request.getRequestDispatcher(
						"success.jsp?username=" + user.getUsername() + "&info="
								+ info + "").forward(request, response);
			}
		}
		if (deal.equals("regist")) {
			boolean flag = db.getUserName(username);
			if (flag) {
				info = "用户已被注册！";
				request.getRequestDispatcher(
						"fail.jsp?info=" + info + "&deal=" + deal + "")
						.forward(request, response);
			} else {
				user.setEmail(email);
				user.setPassword(password);
				user.setUsername(username);
				// System.out.println(username+","+password+","+email);
				db.setUser(user);
				HttpSession session = request.getSession();
				// session.setAttribute("user", user);
				session.setAttribute("user", user);
				info = "注册成功！";
				request.getRequestDispatcher(
						"success.jsp?username=" + username + "&info=" + info
								+ "").forward(request, response);
			}
		}
		if (deal.equals("message")) {
			// HttpSession session=request.getSession();
			// Userbean vuser=(Userbean)session.getAttribute("user");
			// if(vuser==null){
			// info="请先注册";
			// deal="regist";
			// request.getRequestDispatcher("fail.jsp?info="+info+"&deal="+deal+"").forward(request,
			// response);
			// }else{
			ArrayList<Msgbean> msglist = db.getMsg();
			ArrayList<Msgbean> rmsglist = db.getRMsg();
			request.setAttribute("msglist", msglist);
			request.setAttribute("rmsglist", rmsglist);
			request.getRequestDispatcher("index_mess.jsp").forward(request,
					response);
			// }
		}
		if (deal.equals("allmsg")) {
			// HttpSession session=request.getSession();
			// Userbean vuser=(Userbean)session.getAttribute("user");
			// if(vuser==null){
			// info="请先注册";
			// deal="regist";
			// request.getRequestDispatcher("fail.jsp?info="+info+"&deal="+deal+"").forward(request,
			// response);
			// }else{
			int pageno = 1;
			int pagesize = 15;
			if (request.getParameter("pageno").equals("")
					|| request.getParameter("pageno") == null) {
				pageno = 1;
			} else {
				pageno = Integer.parseInt(request.getParameter("pageno"));
			}
			int msgcount = db.getMsgnum();
			ArrayList<Msgbean> allmsg = db.getPageMsg(pageno, pagesize);
			request.setAttribute("allmsg", allmsg);
			request.getRequestDispatcher(
					"listmoreword.jsp?msgcount=" + msgcount + "&pageno="
							+ pageno + "").forward(request, response);
			// }
		}
		if (deal.equals("addmsg")) {
			HttpSession session = request.getSession();
			user = (Userbean) session.getAttribute("user");
			if (user == null) {
				info = "请先注册";
				deal = "regist";
				request.getRequestDispatcher(
						"fail.jsp?info=" + info + "&deal=" + deal + "")
						.forward(request, response);
			} else {
				Msgbean msgb = new Msgbean();
				String msg = request.getParameter("msg");
				SimpleDateFormat format = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				String time = format.format(new Date());
				// System.out.print(uname + "," + msg + "," + time);
				msgb.setUsername(user.getUsername());
				msgb.setMsg(msg);
				msgb.setTime(time);
				int i = db.addMsg(msgb);
				if (i == 0) {
					info = "留言添加失败！";
					request.getRequestDispatcher("addsuc.jsp?info=" + info + "")
							.forward(request, response);
				} else {
					info = "留言添加成功！";
					request.getRequestDispatcher("addsuc.jsp?info=" + info + "")
							.forward(request, response);
				}
			}
		}

		if (deal.equals("buy")) {
			String cid = request.getParameter("cid");
			Clothesbean cb = new Clothesbean();
			cb = db.getClothes(cid);
			if (cb == null) {
				info = "此商品已无库存";
				request.getRequestDispatcher("fail.jsp?info=" + info + "")
						.forward(request, response);
			} else {
				request.setAttribute("clothes", cb);
			}
			request.getRequestDispatcher("price.jsp?cid=" + cid + "").forward(
					request, response);
		}
	}
}
